<?php
	$cMetaDesc = '';
	$cPageTitle = 'Home';
	$cSEOTitle = '';
	$layout = 'home';
	include("header.php");
?>


<main class="container" role="main">
	<div class="row">
		<div class="medium-8 columns">
			<div class="widget general-info-widget col-height">
				<div class="row">
					<div class="large-6 columns right">
						<h1 class="text-right">Hoover Police Department is now hiring!</h1>
						<a href="#" class="button right">More Info</a>
					</div>
				</div>
			</div>
		</div>
		<div class="medium-4 columns">
			<div class="widget facebook-feed-widget col-height">
				<h2>Hoover PD is on Facebook</h2>

			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="medium-4 columns">
			<div class="widget breaking-news-widget">
				<h2 class="has-icon-headline">Breaking News <span><i class="fi-list-thumbnails"></i></span></h2>
				<div class="row">
					<div class="small-12 columns">
						<article>
							<a href="#"><h3>Zero Tolerance for I-459/I-65</h3></a>
							<p><small>Sunday, June 29, 2014</small><br>Hoover PD will be stepping up enforcement on I-459 and I-65.</p>
							<a href="#" class="button button-small">Read More</a>
						</article>
						<hr>
					</div>
				</div><!-- /.row -->
				<div class="row">
					<div class="small-12 columns">
						<article>
							<a href="#"><h3>Robbery Suspects Arrested!</h3></a>
							<p><small>Friday, April 25, 2014</small></p>
							<a href="#" class="button button-small">Read More</a>
						</article>
						<hr>
					</div>
				</div><!-- /.row -->
				<div class="row">
					<div class="small-12 columns">
						<article>
							<a href="#"><h3>Protect Your Lawn Equipment!</h3></a>
							<p><small>Thursday, April 17, 2014</small><br>Now that the weather is warming up, Hoover and surrounding agencies have seen an increase in the number of lawn equipment thefts. </p>
							<a href="#" class="button button-small">Read More</a>
						</article>
					</div>
				</div><!-- /.row -->
				<a href="#" class="button button-widget-tab">See All</a>
			</div><!-- /. widget -->
		</div><!-- /. medium-4 columns breaking-news -->
		<div class="medium-8 columns">
			<div class="row">
				<div class="medium-6 columns">
					<div class="widget online-forms-widget col-height">
						<div class="row">
							<div class="medium-2 columns medium-centered">
								<i class="fi-page-multiple"></i>
							</div>
						</div>
						<h3 class="text-center">Online Forms</h3>
						<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						<a href="#" class="button button-widget-tab">Learn More</a>
					</div><!-- /. widget -->
				</div>
				<div class="medium-6 columns">
					<div class="widget alert-widget col-height">
						<div class="row">
							<div class="medium-2 columns medium-centered">
								<i class="fi-alert"></i>
							</div>
						</div>
						<h3 class="text-center">Alerts</h3>
						<p class="text-center">Citizens living within the City limits of Hoover are eligible to use the Medical Alert System.</p>
						<a href="#" class="button button-widget-tab">Learn More</a>
					</div><!-- /. widget -->
				</div>
			</div><!-- /.row -->
			<div class="row">
				<div class="small-12 columns welcome-message">
					<h3>A Message from Chief Derzis</h3>
					<div class="row">
						<div class="large-4 columns">
							<div class="chief-image">
								<img src="img/derzis.jpg" alt="Chief Derzis" />
							</div>
						</div>
						<div class="large-8 columns">
							<p>Welcome to hooverpd.com, the official website of the Hoover Police Department. <br><br>This website is designed to provide an overview of police personnel and resources used daily in an effort to make Hoover a safe city in which to live, work and visit. This website is just one of the many tools used to help educate our community about the comprehensive police services offered. </p>
							<a class="button right" href="#">Read More</a>
						</div>
					</div><!-- /.row -->
				</div>
			</div><!-- /.row -->
		</div><!-- /.medium-8 columns -->
	</div><!-- /.row -->
	<div class="row">
		<div class="medium-4 columns">
			<div class="widget crime-stoppers-widget col-height">
				<div class="row">
					<div class="medium-2 columns medium-centered">
						<i class="fi-shield"></i>
					</div>
				</div>
				<h3 class="text-center">Crime Stoppers</h3>
				<img src="img/crime-stoppers-widget-img.jpg" alt="Crime Stoppers Logo" />
				<p class="text-center"> Call 205.254.7777</p>
				<a href="#" class="button button-widget-tab">Learn More</a>
			</div>
		</div>
		<div class="medium-4 columns">
			<div class="widget free-app-widget col-height">
				<div class="row">
					<div class="medium-2 columns medium-centered">
						<i class="fi-mobile"></i>
					</div>
				</div>
				<h3 class="text-center">Get the Free App</h3>
				<img src="img/mypd-widget-img.jpg" alt="MyPD Mobile App" />
				<p class="text-center">Available for Apple and Android Devices</p>
				<a href="#" class="button button-widget-tab">Learn More</a>
			</div>
		</div>
		<div class="medium-4 columns">
			<div class="widget misc-info-widget col-height">
				<div class="row">
					<div class="medium-2 columns medium-centered">
						<i class="fi-star"></i>
					</div>
				</div>
				<h3 class="text-center">Hoover PD is Now Hiring!</h3>
				<p class="text-center">Right now is a great time for you to learn about an exciting career in law enforcement with the Hoover Police Department.</p>
				<a href="#" class="button button-widget-tab">Contact A Recruiter Now</a>
			</div>
		</div>
	</div>
</main>

<?php
	include("footer.php");
?>
