<?php
    $cSiteName = 'Hoover Police Department';
    $cSiteCity = 'Hoover';
    $cSiteState = 'AL';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="<?php echo $cMetaDesc; ?>" name="description">
    <meta content="<?php echo $cMetaKW; ?>" name="keywords">
    <title><?php if (!empty($cSEOTitle)) { echo $cSEOTitle; } else if (!empty($cPageTitle)) { ?><?=$cSiteName; ?> – <?php echo $cPageTitle; ?><?php } else { ?><?=$cSiteName; ?> – <?=$cSiteCity; ?>, <?=$cSiteState; ?><?php } ?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="stylesheets/app.css" rel="stylesheet" type="text/css">
    <link href="stylesheets/zeekee-support.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
    <script src="js/modernizr.custom.96079.js"></script>
</head>
<body>
<header role="header">
    <div class="cta-big-header">
        <div class="alert-bar hide-for-small">
            <div class="row">
                <div class="large-9 columns">
                    <ul class="pd-alerts-bar">
                        <span><i class="fi-alert"></i></span>
                        <li>AVOID BECOMING A VICTIM OF FRAUD...Do not sign the back of your credit cards...<a href="#">Read More</a></li>
                        <li>The Hoover Police Department's Financial Crimes Unit are warning residents of a scam...<a href="#">Read More</a></li>
                        <li>Part time job scam is on the rise...<a href="#">Read More</a></li>
                    </ul>
                </div>
                <div class="large-3 columns">
                    <div class="alerts-bar-nav">
                        <ul class="inline-list">
                            <li><button id="alerts-next" data-tooltip data-options="hover_delay:500;" aria-haspopup="true" class="has-tip" title="Next Alert">&#10095;</button></li>
                            <li><button id="alerts-prev" data-tooltip data-options="hover_delay:500;" aria-haspopup="true" class="has-tip" title="Previous Alert">&#10095;</button></li>
                            <li><a href="#" data-reveal-id="expand_alerts"><i class="fi-arrows-expand" data-tooltip data-options="hover_delay:500;" aria-haspopup="true" class="has-tip" title="Expand Alert"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="cta-big-header-inner">
            <!-- CONTACT INFO SMALL ONLY -->
            <div class="hdr-contact-info show-for-small">
                <a href="tel:911"><h2><i class="fi-telephone"></i> Call 911 <small>For Emergencies</small></h2></a>
            </div>
            <!-- / CONTACT INFO SMALL ONLY -->
            <div class="row">
                <div class="medium-3 columns hide-for-small">
                    <div class="hdr-contact-info">
                        <span>for emergencies</span>
                        <h2><i class="fi-telephone"></i> Call 911</h2>
                        <p>or call 205.822.5300</p>
                    </div>
                </div>
                <div class="medium-4 columns medium-offset-1 hide-for-small">
                    <a href="index.php"><figure class="hdr-seal-logo"></figure></a>
                </div>
                <!-- LOGO MAIN SMALL ONLY -->
                <div class="small-8 columns small-centered show-for-small">
                    <div class="push-down"></div>
                    <a href="index.php"><img src="img/HPD-Seal-gold.png" alt="" /></a>
                </div>
                <!-- / LOGO MAIN SMALL ONLY -->
                <div class="medium-3 columns hide-for-small">
                    <form class="hdr-search-bar">
                        <div class="row collapse">
                            <div class="large-10 columns">
                                <input type="search" name="some_name" placeholder="Search">
                            </div>
                            <div class="large-2 columns">
                                <button type="submit" name="some_name" class="hdr-search-bar-button fi-magnifying-glass" value="">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
        <div class="nav-divider"></div>
        <div class="contain-to-grid">
            <nav class="top-bar" data-topbar role="navigation">
                <ul class="title-area">
                    <li class="name">
                        <h1><a href="#"></a></h1>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>
                <section class="top-bar-section">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li class="divider"></li>
                        <li class="has-dropdown"><a href="#">Departmental Info</a>
                            <ul class="dropdown">
                                <li><a href="#">First link in dropdown</a></li>
                                <li class="active"><a href="#">Active link in dropdown</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li class="has-dropdown"><a href="#">Join Us</a>
                            <ul class="dropdown">
                                <li><a href="#">First link in dropdown</a></li>
                                <li class="active"><a href="#">Active link in dropdown</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li class="has-dropdown"><a href="#">Media</a>
                            <ul class="dropdown">
                                <li><a href="#">First link in dropdown</a></li>
                                <li class="active"><a href="#">Active link in dropdown</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li class="has-dropdown"><a href="#">Resources</a>
                            <ul class="dropdown">
                                <li><a href="#">First link in dropdown</a></li>
                                <li class="active"><a href="#">Active link in dropdown</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li class="has-dropdown"><a href="#">Community</a>
                            <ul class="dropdown">
                                <li><a href="#">First link in dropdown</a></li>
                                <li class="active"><a href="#">Active link in dropdown</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </section>
            </nav>
        </div>
    </div>
</header>
<!-- EXPAND ALERTS MODAL -->
<div id="expand_alerts" class="reveal-modal" data-reveal>
    <h3><span><i class="fi-alert"></i></span> Hoover Police Department Alert System</h3>
    <ul class="no-bullet">
        <li>AVOID BECOMING A VICTIM OF FRAUD...Do not sign the back of your credit cards...<a href="#">Read More</a></li>
        <li>The Hoover Police Department's Financial Crimes Unit are warning residents of a scam...<a href="#">Read More</a></li>
        <li>Part time job scam is on the rise...<a href="#">Read More</a></li>
        <a class="close-reveal-modal">&#215;</a>
    </ul>
</div>
<!-- / EXPAND ALERTS MODAL -->
