$(document).foundation();

$('.col-height').matchHeight();

$('.pd-alerts-bar').newsTicker({
    row_height:     35,
    max_rows:       1,
    duration:       10000,
    prevButton:     $('button#alerts-prev'),
    nextButton:     $('button#alerts-next')
});
$.scrollUp({
    scrollName: 'scrollUp', // Element ID
    topDistance: '700', // Distance from top before showing element (px)
    topSpeed: 300, // Speed back to top (ms)
    animation: 'slide', // Fade, slide, none
    animationInSpeed: 200, // Animation in speed (ms)
    animationOutSpeed: 200, // Animation out speed (ms)
    scrollText: 'TOP', // Text for element
    activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
});
